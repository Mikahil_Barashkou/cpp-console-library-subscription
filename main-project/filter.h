#ifndef FILTER_H
#define FILTER_H

#include "it_seminar.h"

it_seminar** filter(it_seminar* array[], int size, bool (*check)(it_seminar* element), int& result_size);


bool check_seminar_by_author(it_seminar* element);



bool check_seminar_by_time(it_seminar* element);


#endif