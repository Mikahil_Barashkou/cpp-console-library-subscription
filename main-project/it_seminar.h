#ifndef IT_SEMINAR_H
#define IT_SEMINAR_H

#include "constants.h"

struct time_s
{
    int hours;
    int minutes;
};

struct person
{
    char first_name[MAX_STRING_SIZE];
    char middle_name[MAX_STRING_SIZE];
    char last_name[MAX_STRING_SIZE];
};

struct it_seminar
{
    time_s start;
    time_s finish;
    person reader;
    char theme_it[MAX_STRING_SIZE];
};

#endif