#include <iostream>
#include <iomanip>

using namespace std;

#include "it_seminar.h"
#include "file_reader.h"
#include "file_reader.cpp"
#include "constants.h"
#include "filter.h"
#include "filter.cpp"

void output (it_seminar* seminar)
{
  /********** вывод времени **********/
            cout << "Начало: ";
            // Вывод времени начала семинара
            cout << seminar->start.hours << ':';
            cout << seminar->start.minutes << ' ';
            // Вывод времени окончания семинара 
            cout << "Конец: ";
            cout << seminar->finish.hours << ':';
            cout << seminar->finish.minutes << ' ';
            // Вывод ФИО лектора семинара
            cout << "Лектор: ";
            cout << seminar->reader.last_name << " ";
            cout << seminar->reader.first_name[0] << ". ";
            cout << seminar->reader.middle_name[0] << ". ";
            // Вывод темы семинара
            cout << "Тема: ";
            cout << seminar->theme_it << '\n';
            cout << '\n';
}

int main()
{

    setlocale(LC_ALL, "Russian");
    cout << "Лабораторная работа №8. GIT\n";
    cout << "Вариант №2. Программа конференции\n";
    cout << "Автор: Михаил Барашков\n\n";
    it_seminar* seminar[MAX_FILE_ROWS_COUNT];
    int size;
    try
    {
        read("data.txt", seminar, size);
        cout << "***** Программа семинара *****\n\n";
        for (int i = 0; i < size; i++)
        {
            output(seminar[i]);
        }
        bool (*check_function)(it_seminar*) = NULL; 
		cout << "\nВыберите способ фильтрации или обработки данных:\n";
		cout << "1) Вывести все доклады Иванова Ивана Ивановича.\n";
		cout << "2) Вывести все доклады длительностью больше 15 минут.\n";
		cout << "\nВведите номер выбранного пункта: ";
		int item;
		cin >> item;
		cout << '\n';
		switch (item)
		{
		case 1:
			check_function = check_seminar_by_reader;
			cout << "***** Доклады Иванова Ивана Ивановича *****\n\n";
			break;
		case 2:
			check_function = check_seminar_by_time; 
			cout << "***** Доклады длительностью больше 15 минут *****\n\n";
			break;
		default:
			throw "Некорректный номер пункта";
		}
		if (check_function)
		{
			int new_size;
			it_seminar** filtered = filter(seminar, size, check_function, new_size);
			for (int i = 0; i < new_size; i++)
			{
				output(filtered[i]);
			}
			delete[] filtered;
		}
        for (int i = 0; i < size; i++)
        {
            delete seminar[i];
        }
    }
    catch (const char* error)
    {
        cout << error << '\n';
    }

    return 0;
}