#include "file_reader.h"
#include "constants.h"

#include <fstream>
#include <cstring>

time_s convert(char* str)
{
    time_s result;
    char* context = NULL;
    char* str_number = strtok_r(str, ":", &context);
    result.hours = atoi(str_number);
    str_number = strtok_r(NULL, ":", &context);
    result.minutes = atoi(str_number);
    return result;
}

void read(const char* file_name, it_seminar* array[], int& size)
{
    std::ifstream file(file_name);
    if (file.is_open())
    {
        size = 0;
        char tmp_buffer[MAX_STRING_SIZE];
        while (!file.eof())
        {
            it_seminar* item = new it_seminar;
            file >> tmp_buffer;
            item->start = convert(tmp_buffer);
            file >> tmp_buffer;
            item->finish = convert(tmp_buffer);
            file >> item->reader.last_name;
            file >> item->reader.first_name;
            file >> item->reader.middle_name;
            file.read(tmp_buffer, 1); // чтения лишнего символа пробела
            file.getline(item->theme_it, MAX_STRING_SIZE);
            array[size++] = item;
        }
        file.close();
    }
    else
    {
        throw "Ошибка открытия файла";
    }
}