#include "filter.h"
#include "it_seminar.h"
#include <cstring>
#include <iostream>


it_seminar** filter(it_seminar* array[], int size, bool (*check)(it_seminar* element), int& result_size)
{
	it_seminar** result = new it_seminar*[size];
	result_size = 0;
	for (int i = 0; i < size; i++)
	{
		if (check(array[i]))
		{
			result[result_size++] = array[i];
		}
	}
	return result;
}

bool check_seminar_by_reader(it_seminar* element)
{
	return strcmp(element->reader.first_name, "Иван") == 0 &&
	       strcmp(element->reader.middle_name, "Иванович") == 0 &&
	       strcmp(element->reader.last_name, "Иванов") == 0;
}

bool check_seminar_by_time(it_seminar* element)
{
	return element->start.hours == element->finish.hours && element->finish.minutes > element->start.minutes + 16 ||
    element->start.hours < element->finish.hours && element->finish.minutes + 60 - element->start.minutes > 15;
}
