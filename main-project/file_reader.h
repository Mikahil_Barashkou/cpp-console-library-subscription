#ifndef FILE_READER_H
#define FILE_READER_H

#include "it_seminar.h"

void read(const char* file_name, it_seminar* array[], int& size);

#endif